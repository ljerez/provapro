Math.randint = function (max, min = 0) {
  min = this.ceil(min);
  max = this.floor(max);
  return this.floor(this.random() * (max-min)) + min;
}


Object.prototype.keys = function () {
  return Object.keys(this);
}
game=(()=>{
  const canvas = document.getElementById('stage') // Canvas riportato nel file js
  const ctx = canvas.getContext("2d");


  const colors = {
    'red': -30,
    'blue': -10,
    'green': -20,
    'black': -100
  };

  const bomb = new Audio('assets/bomb.mp3');
  const fire = new Audio('assets/fire.mp3');
  const grenade = new Audio('assets/grenade.mp3');





  ///////////////////////////////////////////////////////////////////
  // S P R I T E
  //////////////////////////////////////////////////////////////////

  class Sprite { // Classe generica si base
    constructor(src , x=0, y=0, w=0, h=0,vx = 0, vy=0) {
      this.img = new Image();
      this.img.src = src;
      this.x = x;
      this.y = y;
      this.width = w;
      this.height = h;
      this.vx = vx;
      this.vy = vy;
      this.alive = true;


    }
    update (time) {
      this.x += this.vx;
      this.y += this.vy;
    }

    draw (time) {
      ctx.drawImage(this.img,  this.x, this.y, this.width, this.height);

      ctx.beginPath();
      ctx.rect(this.x, this.y, this.width, this.height);
      ctx.stroke();

    }
    // Se non separati in orrizontale e non separati in verticale
    collides(obj) {
      return !(obj.x + obj.width < this.x || this.x + this.width < obj.x) &&
        !(obj.y + obj.height < this.y || this.y + this.height < obj.y);
    }
  }

  ////////////////////////////////////////
  // B U L L E T
  ///////////////////////////////////////

  class Bullet extends Sprite {
    constructor(x, y) {
      super("assets/bullet.png", x, y, 29, 27, 14, 0 );
      //this.bulletTime = time + 1;
    }
     update(time) {
        super.update(time);
        if (this.x > canvas.offsetWidth) {
         this.alive = false;
         console.log("Balloob dead");
        }
     }
  }



  ////////////////////////////////////////
  // P L A N E
  ///////////////////////////////////////
  class Plane extends Sprite {
    constructor() {
      super("assets/Fly_1.png", 100, 100, 148, 101, 5, 5);
      this.sources = ["assets/Fly_1.png", "assets/Fly_2.png"];
      this.currentImage = 0;
      this.deltaAnimation = 50; // Milli secondo, ogni quanto cambiare immagine
      this.lastChange = 0;
      this.mouse = {x: 100, y:100};
      this.speed = 5;

      canvas.addEventListener('mousemove', event => {
        this.mouse.x = event.offsetX - this.width/2;
        this.mouse.y = event.offsetY - this.height/2;
      });
    }
    update(time) {
      /*
      if (Math.abs(this.mouse.x - this.x) >= this.vx) {
        this.x += Math.sign(this.mouse.x - this.x) * this.vx;
      }
      if (Math.abs(this.mouse.y - this.y) >= this.vy) {
        this.y += Math.sign(this.mouse.y - this.y) * this.vy;
      }
      */
      if (this.mouse) {
        var a = Math.atan2(this.mouse.y - this.y, this.mouse.x - this.x);
        var r = Math.sqrt((this.mouse.y - this.y)**2+(this.mouse.x-this.x)**2);
        if (r<= this.speed) {
          this.x = this.mouse.x;
          this.y = this.mouse.y;
        } else {
          this.x += this.speed * Math.cos(a);
          this.y += this.speed * Math.sin(a);
        }
      }


      if (time - this.lastChange > this.deltaAnimation) {
        //this.currentImage = 1 - this.currentImage;
        this.currentImage =  ++this.currentImage % this.sources.length; // Caso generale
        this.lastChange = time;
        this.img.src = this.sources[this.currentImage];
      }
      //console.log(this.mouse);
    }
    draw(time) {
      super.draw(time);
    }
  }


  ////////////////////////////////////////
  //S T A G E
  ///////////////////////////////////////
  // Disegna lo sfondo e gli sprite;
  // Aggiorna sfondo e sprite

  class Stage extends Sprite{
    constructor(w = 800, h = 400) {
      super("assets/bg.png", 0, 0, w, h, -5, 0);
      //this.sprites = [];  // To do: mettere la lista
      this.balloons = new ListFactory();
      this.bullets = new ListFactory();
      this.plane = new Plane();
      this.score = 0;
      this.nbullets = 0;
      this.motor = new Audio('assets/airplane.mp3');
      this.motor.loop = true;
      this.motor.volume = .3;
    }


    start() {
      window.addEventListener('keydown', event=> {
        if (this.nbullets > 0) {
          this.nbullets--;
          this.bullets.push(new Bullet(this.plane.x+96, this.plane.y + 63));
          fire.cloneNode().play();
        }
      });
      this.motor.play();
    }

    stop() {
      this.motor.pause();
      window.removeEventListener('keydown');

    }
    // Aggiorna lo stato dell'oggetto
    update(time) {
      super.update(time);
      if (this.x <= -this.width) {
        this.x = 0;
      }
      // Update degli sprite
      this.balloons.visit(b => b.update(time));
      this.bullets.visit(b => b.update(time, this.width, this.height));
      this.plane.update(time);



      // Collision detection
      this.balloons.map(
        b => {
          if (this.plane.collides(b)) {
            b.alive = false;
            this.score += colors[b.color];
            if (b.color === 'black') {
              this.nbullets -= 3;
            } else if (b.color === 'green') {
              this.nbullets += 2;
            } else if (b.color === 'blue') {
              this.nbullets -= 2;
            } else if (b.color === 'red') {
              this.nbullets -= 1;
            }
            if (this.nbullets < 0) {
              this.nbullets = 0;
            }
            bomb.cloneNode().play();
          }
          return b;
        }
      );

      this.bullets.map(
        v => {

          this.balloons.visit(
            b => {
              if (v.collides(b)) {
                b.alive = false;
                v.alive = false;
                this.score += -colors[b.color];
                if (b.color === 'black') {
                  this.nbullets += 2;
                } else if (b.color === 'green') {
                  this.nbullets += 2;
                } else if (b.color === 'blue') {
                  this.nbullets += 1;
                } else if (b.color === 'red') {
                  this.nbullets += 1;
                }
                grenade.cloneNode().play();
              }
            });
          return v;
        }
      );

      // Amministrazione morti
      this.balloons = this.balloons.reduce(
        (b, a) => {if (b.alive) a.push(b); return a;},
        new ListFactory()
      );

      this.bullets = this.bullets.reduce(
        (b, a) => {if (b.alive) a.push(b);
        return a;},
        new ListFactory()
      );


      // Balloon spawning
      if (this.balloons.size() < 20) {
        if (Math.random() > 0.95) {
          let c = colors.keys()[Math.randint(4)];
          this.balloons.push(new Balloon(Math.random()*750+50, 400, c));
          //console.log("Balloon spawned");
        }
      }


    }


    // Disegna l'oggetto sul canvas
    draw(time) {

      super.draw(time);
      ctx.drawImage(this.img,  this.x + this.width, 0);
      // Disegno degli sprite
      this.plane.draw(time);
      this.balloons.visit(b=> b.draw(time));
      this.bullets.visit(b=> b.draw(time));

      // Aggioornamenti interfaccia
      document.getElementById('timer').innerHTML =
        (time/1000).toFixed(2).toString().replace('.', ':') ;
      document.getElementById('score').innerHTML = this.score;
      document.getElementById('cval').innerHTML = this.nbullets;
    }
  }
  //////////////////////////////////////////////////////////
  // B A L L O O N S
  /////////////////////////////////////////////////////////
  class Balloon extends Sprite{
    constructor(x = 0, y = 0, color = 'blue') {
      super(`assets/balloon_${color}.png`, x, y, 51, 64, -3, -3);
      this.color = color;
    }

    update(time) {
      super.update(time)
      this.y +=  (Math.random() -0.5) * 3;
      this.x += (Math.random() -0.5) * 6;
      // Se fuori schermo
      if (this.y < -this.height || this.x < -this.width) {
        //console.log(this.height, this.width);
        this.alive = false;
        //console.log("Balloon dead");
      }
    }

  }

  //////////////////////////////////////////////////////////
  // M A I N
  /////////////////////////////////////////////////////
  class Game {
    constructor() {
      this.currentTime = 0;
      this.startTime = null;
      this.btnstart=document.getElementById('start');
    }
    start() {
      this.stage = new Stage();

      document.getElementById('start').style.display = 'none';
      this.currentTime = 0;
      this.startTime = null;
      this.stage.score = 0;
      this.stage.nbullets = 12;
      this.stage.start();
    }

    step (time) {
      if (!this.startTime) {
        this.startTime = time;
      }
      this.currentTime = time - this.startTime;
      this.stage.update(this.currentTime);
      this.stage.draw(this.currentTime);
    }
    stop () {
      document.getElementById('start').style.display = '';
      this.stage.stop();
    }
  }

  ///////////////////////////////////////////////////////////////////////////////////



  const game = new Game();
  const btnstart=document.getElementById('start');


  // Animation loop
  function run(time) {
    game.step(time);
    if (game.currentTime < 8000) {
      setTimeout(step, 0);
    } else {
     game.stop();
    }

  }
  function step(){
    requestAnimationFrame(run);
  }
  function start() {
    game.start();
    step();
  }
  btnstart.addEventListener("click",start);
  return game;
})();


/*
function gigio(){
  alert('ciao');
}
gigio();
*/
